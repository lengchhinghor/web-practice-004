import React from 'react'
import {Card,Button} from 'react-bootstrap'

function Item(props) {

    let {img,title,description,show} = props.data
    let item = props.item

    return (
        <Card>
            <Card.Img variant="top" src={img} />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                {description}
                </Card.Text>

            </Card.Body>
        </Card>
    )
}



export default Item
