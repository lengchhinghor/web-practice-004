import { Col, Container, Row,Button } from 'react-bootstrap';
import './App.css';
import Item from './components/Item';
import NavMenu from './components/NavMenu';
// import {Card,} from 'react-bootstrap'



import React, { Component } from 'react'

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          img: 'img/angkor.jpeg',
          title: 'Angkor',
          description: 'In cambodia',
          show: false
        },
        {
          img: 'https://www.visitsoutheastasia.travel/wp-content/uploads/2019/10/Cambodia-Taprom-Temple-Siem-Reap.jpg',
          title: 'Taprom',
          description: 'In cambodia',
          show: true

        },
        {
          img: 'https://juliasalbum.com/wp-content/uploads/2019/07/Banteay-Srei-Lady-Temple-Cambodia-16.jpg',
          title: 'Banteay Srey',
          description: 'In cambodia',
          show: true

        }
      ]
    }

  }
  delete(item){
    const newState = this.state.data.slice();
    if(newState.indexOf(item) > -1) {
      newState.splice(newState.indexOf(item), 1);
      this.setState({data: newState})
    }
  }

  render() {
   
    return (
      <>
        <NavMenu />
        <Container>
          <Row>
            { 
              this.state.data.map((obj, idx) =>
                <Col key={idx} md={3} sm={6}>
                  <Item data={obj} />
                  <Button 
                  onClick={this.delete.bind(this, obj)}
                  variant="danger">Delete</Button>
                </Col>)
            }
            
          </Row>
        </Container>
      </>
    )
  }
}
